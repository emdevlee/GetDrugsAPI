# druglookup

druglookup is a web application built to transform data from the openFDA API to make it easier to consume on the frontend.

![image info](./pics/GetDrugsAPI.png)


## Introduction
The openFDA API provides endpoints to retrieve useful drug data however, the responses can be very large and have nested fields. The primary goal of this project was to demonstrate the simplification through a full-stack application that pulls in the transformed data.

## Technologies Used

- Backend: Maven 3.9.6, Java 17, Spring Boot 3.3.1, Docker 24.0
- APIs: openFDA API

## Features

- Retrieve and transform data from the openFDA API to a flatten response.

## Local Setup and Installation

The application utilizes docker to setup the environment as seen in the `docker-compose-local.yaml`

```
version: '3.8'

services:
  thedrugapi-backend:
    build:
      context: ./backend
      dockerfile: Dockerfile
    networks:
      - drugnet
    ports:
      - '8080:8080'
networks:
  drugnet:
    driver: bridge
```


1. Change directories to the `backend` folder and run `mvn clean install` to produce the `.jar` file to be run in the docker container
2. Change directories back to the root of the project and run `docker compose -f docker-compose-local.yaml up -d` to create the containers

## Live Site
A demo frontend was built to showcase the API in production [here](https://druglookup.netlify.app). The frontend is hosted on Netlify while the backend is in a kubernetes cluster hosted on an Oracle compute instance behind an NGINX reverse proxy. 

### Examples
![image](https://github.com/michaeldevlee/GetDrugsAPI/assets/58196525/6221fafe-4362-4d2e-8d0b-c85e81eb057f)
![image](https://github.com/michaeldevlee/GetDrugsAPI/assets/58196525/1faa1b4d-4863-46ff-8fc7-50c7a4d8b5b5)
![image](https://github.com/michaeldevlee/GetDrugsAPI/assets/58196525/27977113-3eb2-41d3-9b04-19476b13a4cd)


