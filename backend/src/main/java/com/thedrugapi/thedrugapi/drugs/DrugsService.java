package com.thedrugapi.thedrugapi.drugs;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public interface DrugsService{
    ArrayList<DrugDTO> getDrugs(String moa, String generic_name, String brand_name, int page, int size, Boolean reverse);
}
