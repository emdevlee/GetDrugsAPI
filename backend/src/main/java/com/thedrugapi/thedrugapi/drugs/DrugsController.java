package com.thedrugapi.thedrugapi.drugs;

import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
@RequestMapping("/drug")
public class DrugsController {

    private final DrugsService drugsService;

    @GetMapping
    public ResponseEntity<Object> getDrugList(
        @RequestParam (value = "moa", defaultValue = "") String moa,
        @RequestParam (value = "generic_name", defaultValue = "") String generic_name,
        @RequestParam (value = "brand_name", defaultValue = "") String brand_name,
        @RequestParam (value = "page", defaultValue = "1") int page,
        @RequestParam (value = "size", defaultValue = "1") int size,
        @RequestParam (value = "asc", defaultValue = "true") Boolean asc
        ) 
    {
        ArrayList<DrugDTO> drugList = drugsService.getDrugs(
            moa, 
            generic_name, 
            brand_name, 
            page, 
            size,
            asc
            );
        return ResponseEntity.ok(drugList);
    }
    
}
