package com.thedrugapi.thedrugapi.drugs;

import java.util.ArrayList;


import lombok.Getter;

@Getter
public class Results {
    private ArrayList<Result> results;
}
